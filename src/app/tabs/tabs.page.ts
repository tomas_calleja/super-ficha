import { Component  } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  
  ngOnInit(){
    this.GetData()
  }
  data: string;
  constructor(private http: HttpClient) {
    this.data = "uno"
  }

  GetData() {
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append("Access-Control-Allow-Origin", "*");
    headers.append('Access-Control-Allow-Credentials', "true");

    // this.http.get("https://kdvf3hiqo9.execute-api.eu-west-1.amazonaws.com/Prod/pj/vero", {headers: headers})
    //     .subscribe(data => {
    //         console.log(data);
    //     }, error => {
    //         console.log(error);
    //     });
    // this.http.get("http://dummy.restapiexample.com/api/v1/employees", {headers: headers})
    //     .subscribe(data => {
    //         console.log(data);
    //         this.data = JSON.stringify(data);
    //     }, error => {
    //         console.log(error);
    //     });
}

}
