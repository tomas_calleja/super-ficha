import { Component } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  ngOnInit(){
    console.log("cargando")
    const myParams: object = this.route.snapshot;
    console.log("myParams",myParams)
    this.sheetId = this.route.snapshot.queryParamMap.get('sheet');
    // const secondParam: string = this.route.snapshot.queryParamMap.get('secondParamKey');
    this.GetData()

  }
  sheetId: string
  data: object;
  charData: object;
  loaded: boolean;
  general: boolean;
  caracteristicas: boolean;
  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this.data = {};
    this.charData = {};
    this.loaded = false;
    this.caracteristicas = false;
    this.general = false;
  }

  GetData() {
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append("Access-Control-Allow-Origin", "*");
    headers.append('Access-Control-Allow-Credentials', "true");

    let url = "https://spreadsheets.google.com/feeds/cells/1YgHDT5bNuoORoFvuygzPTBCdkub34n72vq3gcmwQz_s/1/public/values?alt=json"
    if (this.sheetId){
      url = `https://spreadsheets.google.com/feeds/cells/${this.sheetId}/1/public/values?alt=json`
    }

    this.http.get(url, {headers: headers})
        .subscribe(data => {
            this.loaded = true
            //console.log(data);
            this.data['nombre'] = data['feed']['entry'][4]['content']['$t'];
            this.data['humano'] = data['feed']['entry'][5]['content']['$t'];
            this.data['desc'] = data['feed']['entry'][6]['content']['$t'];
            this.data['foto'] = data['feed']['entry'][7]['content']['$t'];
            let tempCharData = {};
            for (let index = 0; index < data['feed']['entry'].length; index++) {
              const element = data['feed']['entry'][index]['gs$cell'];
              if(element.col === "1"){
                tempCharData[element.row] = {prop: element['$t']}
              }else if(element.col !== "1"){
                tempCharData[element.row][`value_${element.col}`] = element['$t']
              }
            }
            for (const key in tempCharData) {
              if (tempCharData.hasOwnProperty(key)) {
                const element = tempCharData[key];
                this.charData[element.prop] = element;
              }
            }
            console.log("datos limpios", this.charData)
        }, error => {
            console.log(error);
        });
    }
}
